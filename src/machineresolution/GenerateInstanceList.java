package machineresolution;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import tools.Node;
import tools.ProcessFile;
import tools.Tree;

// This class generates an instance list which contains all the instances of event anaphora required for
// training of machine
// It takes path to files containing the fully parsed files as argument

public class GenerateInstanceList 
{
	public ArrayList<HashMap<String, String>> instanceList = new ArrayList<HashMap<String, String>>();
	
	public GenerateInstanceList(String path)
	{
		File f1 = new File(path);
		
		String[] fileList = f1.list();
		
		instanceList = new ArrayList<HashMap<String, String>>(); 
		
		for(int i = 0; i < fileList.length; i++)
		{
			String filename = fileList[i];
			//System.out.printf("%d : %s\n",i,filename);
			File f2 = new File(f1.getAbsoluteFile() + "/" + filename);
			ProcessFile pf = new ProcessFile(f2);
			
			ArrayList<Integer> keys = pf.Keys;
			HashMap<Integer, Tree> oneFile = pf.ProcessedFile;
			
			for(int j = 0; j < keys.size(); j++)
			{
				int sent_id = keys.get(j);
				//System.out.println("Sentence : " + sent_id);
				Tree stree = oneFile.get(sent_id);
				
				// Analyse the treee to get all the features required.
				
				ArrayList<Node> nodeList = stree.findAll("reftype", "E");
				
				for(int k = 0; k < nodeList.size(); k++)
				{
					Node node = nodeList.get(k);
					
					//System.out.printf("%d\t%s\n",sent_id,node.getName());
					
					boolean yehki = false;
					
					ArrayList<String> children = node.getChildren();
					for(int l = 0; l < children.size(); l++)
					{
						String s = children.get(l);
						if(stree.getNode(s).getPOS().equals("CCP") && stree.getNode(s).getDrel().equals("rs"))
						{
							yehki = true;
							break;
						}
					}
					
					if(!yehki)
					{
						int ana_posn = node.getPosn();
					
						HashMap<String, String> ana_feat = getAnaFeat(node);
					
						HashMap<String, String> word = node.getWordOfType("PRP");
					
						String ref = word.get("ref");
					
						String ref_chunk_name = null;
						int ref_sent_id = 0;
					
						String ref_chunk_name_2 = null;
						int ref_sent_id_2 = 0;
					
						if(ref.contains("/"))
						{
							if(ref.contains(".."))
							{
								String[] sref = ref.split("%|/");
								ref_chunk_name = sref[2];
								ref_sent_id = Integer.valueOf(sref[1]);
								ref_chunk_name_2 = sref[5];
								ref_sent_id_2 = Integer.valueOf(sref[4]);
							
							}
							else
							{
								String[] sref = ref.split("/");
								ref_chunk_name = sref[0];
								ref_sent_id = sent_id;
								ref_chunk_name_2 = sref[1];
								ref_sent_id_2 = sent_id;
							}
						}
						else
						{
							if(ref.contains(".."))
							{
								String[] sref = ref.split("%");
								ref_chunk_name = sref[2];
								ref_sent_id = Integer.valueOf(sref[1]);
							}
							else
							{
								ref_chunk_name = ref;
								ref_sent_id = sent_id;	
							}
						}
						//System.out.printf("%d\t%s\n",ref_sent_id,ref_chunk_name);
					
						// GO three sentences back and start collecting verbs
						
						int n = 0;
						ArrayList<HashMap<String, String>> instances = new ArrayList<HashMap<String, String>>();
						int nearest = 0;
						for(int l = ref_sent_id; l <= sent_id; l++)
						{
							
							
							int sent_dis = sent_id - ref_sent_id;
							//System.out.printf("%d\t%d\n", sent_id, sent_dis);
							Tree vsent = oneFile.get(l);
							// this takes care of missing sentence ids
							if(vsent == null) vsent = oneFile.get(++l);
							if(vsent == null) vsent = oneFile.get(++l);
							if(vsent == null) vsent = oneFile.get(++l);
							
							//System.out.printf("%d\t%d\n",sent_id,l);
							
							int size = vsent.getKeys().size();
							for(int m = 1; m <= size; m++)
							{
								//System.out.println("Node : " + m);
								Node vnode = vsent.getNode(m);
							
								if(vnode.getPOS().contains("VG") && !vnode.getPOS().contains("NULL"))
								{
									
									//System.out.printf("\t\t%d\t%d\n",ana_posn,vnode.getPosn());
									
									if(l < sent_id || (l == sent_id && vnode.getPosn() < ana_posn)) { nearest = n;}//System.out.println("nearest : " + nearest);}
									
									HashMap<String, String> verb_feat = getVerbFeat(vnode);
									HashMap<String, String> oneinstance = new HashMap<String, String>();
								
									oneinstance.putAll(ana_feat);
									oneinstance.putAll(verb_feat);
								
									oneinstance.put("sent_dis", String.valueOf(sent_dis));
								
									if(l == sent_id && vnode.getName().equals(ref_chunk_name))
										oneinstance.put("class", "true");
									else
										oneinstance.put("class", "false");
								
									instances.add(oneinstance);
									n++;
									
								}
							}
						}
						
						for(int l = 0; l < instances.size(); l++)
						{
							if(l <= nearest) instances.get(l).put("verb_dis", String.valueOf(nearest - l + 1));
							else instances.get(l).put("verb_dis", String.valueOf(nearest - l));
							
							//System.out.printf("%s\t%s\t%s\n",instances.get(l).get("ref_word"),instances.get(l).get("verb_dis"),instances.get(l).get("class"));
						}
						//System.out.println();
						instanceList.addAll(instances);
					}					
				}
			}
		}		
	}

	
	private HashMap<String, String> getAnaFeat(Node node) 
	{
		HashMap<String, String> ana_feat = new HashMap<String, String>();
		
		int ana_numEle = node.getNumEle();
		ana_feat.put("ana_numEle", String.valueOf(ana_numEle));
		
		int ana_numChild = node.getChildren().size();
		ana_feat.put("ana_numChild", String.valueOf(ana_numChild));
		
		String ana_drel = node.getDrel();
		ana_feat.put("ana_drel", ana_drel);
		
		String ana_semprop = node.getSemprop();
		ana_feat.put("ana_semprop", ana_semprop);
				
		HashMap<String, String> anafeat = node.getWordOfType("PRP");
		HashMap<String, String> nextfeat = node.getNext();
		
		String anaphora = anafeat.get("word");
		ana_feat.put("anaphora",anaphora);
		
		String ana_root = anafeat.get("root");
		ana_feat.put("ana_root", ana_root);
		
		String ana_gender = anafeat.get("gender");
		ana_feat.put("ana_gender", ana_gender);
		
		String ana_num = anafeat.get("num");
		ana_feat.put("ana_num", ana_num);
		
		String ana_person = anafeat.get("person");
		ana_feat.put("ana_person", ana_person);
		
		String ana_case = anafeat.get("case");
		ana_feat.put("ana_case", ana_case);
		
		String ana_TAM = anafeat.get("TAM");
		ana_feat.put("ana_TAM", ana_TAM);
		
		
		if(nextfeat != null)
		{
			String ana_next = nextfeat.get("word");
			ana_feat.put("ana_next", ana_next);
		}
		else
			ana_feat.put("ana_next", null);
		
		return ana_feat;
	}

	private HashMap<String, String> getVerbFeat(Node verbnode) 
	{
		HashMap<String, String> verb_feat = new HashMap<String, String>();
		int ref_numEle = verbnode.getNumEle();
		verb_feat.put("ref_numEle", String.valueOf(ref_numEle));
		
		int ref_numChild = verbnode.getChildren().size();
		verb_feat.put("ref_numChild", String.valueOf(ref_numChild));
		
		String ref_drel = verbnode.getDrel();
		verb_feat.put("ref_drel", ref_drel);
		
		String ref_semprop = verbnode.getSemprop();
		verb_feat.put("ref_semprop", ref_semprop);
		
		HashMap<String, String> verb = verbnode.getWordOfType("VM");
		
		//System.out.println(verb);
		String ref_word = verb.get("word");
		verb_feat.put("ref_word", ref_word);
		
		String ref_root = verb.get("root");
		verb_feat.put("ref_root", ref_root);
		
		String ref_gender = verb.get("gender");
		verb_feat.put("ref_gender", ref_gender);
		
		String ref_num = verb.get("num");
		verb_feat.put("ref_num", ref_num);
		
		String ref_case = verb.get("case");
		verb_feat.put("ref_case", ref_case);
		
		String ref_person = verb.get("person");
		verb_feat.put("ref_person", ref_person);
		
		String ref_TAM = verb.get("TAM");
		verb_feat.put("ref_TAM", ref_TAM);
		
		return verb_feat;
	}
}