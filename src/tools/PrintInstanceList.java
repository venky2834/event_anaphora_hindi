package tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PrintInstanceList 
{
	public static void print(ArrayList<HashMap<String, String>> instanceList)
	{
		HashMap<String, String> inst = instanceList.get(0);
		Set<String> keyName = inst.keySet();
		Iterator<String> it = keyName.iterator();
		
		while(it.hasNext())
		{
			System.out.print(it.next() + "\t");
		}
		System.out.println();
		
		for(int i = 0; i < instanceList.size(); i++)
		{
			HashMap<String, String> instance = instanceList.get(i);
			Set<String> keys = instance.keySet();
			Iterator<String> iter = keys.iterator();
			
			while(iter.hasNext())
			{
				System.out.print(instance.get(iter.next()) + "\t");
			}
			System.out.println();
		}
	}
}
