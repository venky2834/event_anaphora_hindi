package test;

import java.util.ArrayList;
import java.util.HashMap;

import tools.ClassAttributeList;
import weka.classifiers.meta.FilteredClassifier;
import classification.GenerateInstanceList;
import classification.Train;

public class TestTrain 
{
	public static void main(String[] args) throws Exception
	{
		GenerateInstanceList gil = new GenerateInstanceList("annotated");
		ArrayList<HashMap<String,String>> allInstances = gil.instanceList;
		
		HashMap<String, Boolean> attrib = ClassAttributeList.get();
		
		String pathToArff = "arff/classify_train.arff";
		String pathStr = "arff/classify_structure.arff";
		
		Train train = new Train(allInstances,pathToArff, pathStr, attrib);
		
		FilteredClassifier fc = train.fc;
	}

}