package ruleresolution;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import tools.Node;
import tools.ProcessFile;
import tools.Tree;

//
public class AntecedentStats {
	
	// this class prints the sentence distance and anaphora and antecedent sentence_id and chunk_name
	
	public static void main(String[] args) 
	{
		// All the files are present in this directory
		String path = "annotated";
		File f1 = new File(path);
		
		String[] fileList = f1.list();
		
		
		for(int i = 0; i < fileList.length; i++)
		{
			// For a single file
			String filename = fileList[i];
			//System.out.println(filename);
			File f2 = new File(f1.getAbsoluteFile() + "/" + filename);
			ProcessFile pf = new ProcessFile(f2);
			
			ArrayList<Integer> keys = pf.Keys;
			HashMap<Integer, Tree> oneFile = pf.ProcessedFile;
			
			for(int j = 0; j < keys.size(); j++)
			{
				// for a single sentence
				int sent_id = keys.get(j);
				//System.out.println("Sentence :" + sent_id);
				Tree stree = oneFile.get(sent_id);
				// Analyse the treee to get all the features required.
				
				ArrayList<Node> nodeList = stree.findAll("reftype", "E");
				
				for(int k = 0; k < nodeList.size(); k++) 
					{
						Node n = nodeList.get(k);
						HashMap<String, String> word = n.getWordOfType("PRP");
						//System.out.println(word.get("ref"));
						int ref_sent_id = 0;
						String ref_chunk_name = null;
						
						int ref_sent_id_2 = 0;
						String ref_chunk_name_2 = null;
						
						String ante = word.get("ref");
						if(ante.contains(".."))
						{
							Scanner scan = new Scanner(ante);
							scan.useDelimiter("%|/|,");
							ArrayList<String> al = new ArrayList<String>();
							while(scan.hasNext()) al.add(scan.next());
							scan.close();
							ref_sent_id = Integer.valueOf(al.get(1));
							ref_chunk_name = al.get(2);
							if(ante.contains("/"))
							{
								ref_sent_id_2 = Integer.valueOf(al.get(4));
								ref_chunk_name_2 = al.get(5);
							}
						}
						else
						{
							ref_sent_id = sent_id;
							if(ante.contains("/"))
							{	
								Scanner sc = new Scanner(ante);
								sc.useDelimiter("/");
								ArrayList<String> al = new ArrayList<String>();
								while(sc.hasNext()) al.add(sc.next());
								sc.close();
								ref_sent_id_2 = sent_id;
								ref_chunk_name = al.get(0);
								ref_chunk_name_2 = al.get(1);
							}
							else
							{
								ref_chunk_name = ante;
							}
						}
						boolean yehki = false;
						if((sent_id - ref_sent_id) == 0)
						{
							ArrayList<String> children = n.getChildren();
							for(int l = 0; l < children.size(); l++)
							{
								Node child = stree.getNode(children.get(l));
								if(child.getPOS().equals("CCP") && child.getDrel().equals("rs"))
									{
										yehki = true;
										break;
									}
							}
							if(!yehki) System.out.printf("%d\t%d\t%s\t%d\t%s\t%d\n", sent_id,ref_sent_id,ref_chunk_name,ref_sent_id_2,ref_chunk_name_2, sent_id - ref_sent_id);
						}
						else System.out.printf("%d\t%d\t%s\t%d\t%s\t%d\n", sent_id,ref_sent_id,ref_chunk_name,ref_sent_id_2,ref_chunk_name_2, sent_id - ref_sent_id);
					}
			}
			
			
		}
		
	}
}