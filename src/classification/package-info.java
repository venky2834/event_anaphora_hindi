/**
 * Project : Event Anaphora Resolution
 * 
 * Project Mentor : Praveen Dhakwale
 * 
 * Author : Venkatesh Shukla, Anjali Jain
 * 
 * Language Technology and Research Centre , International Institute of Information Technology
 * 
 * This Package contains the classes used in the classification of Anaphora as Event Anaphora or Entity Anaphora
 * 1. AnaphoraYeh.java - this file both test and trains on three different sets to get results of classification of anaphora with root yeh
 * 2. Template.java - This file extracts an ArrayList of all instances of occurrence of Pronoun having "यह" as root, non - UNK and non - DEM and non NULL
 * 3. GenerateInstanceList - This file generates an ArrayList<HashMap<String, String>> containing all instances required for training for classification.
 * 4. MachineLearn - This file takes in the path to Arff file and makes a FilteredClassifier after machine learning. 
 * 5. Train.java - This file creates both the data arff as well as structure arff file for training and testing for anaphora classification using machine learning approach.
 *		It takes in 4 arguments:
 *		1. ArrayList<HashMap<String, String>> trainList - a list of all the instances to be used for training.
 *		2. String pathToArff - path to arff file to be generated for training
 *		3. String pathStr - path to structure arff file to be generated for testing.
 *		4. HashMap<String, Boolean> attribs - HashMap of attributes to be included during training and testing.
 * 6. Test.java - This file tests the testing instances and using the filteredclassifier predicts the class of instance. Using the actual class, results are obtained.
 * 		It takes in 4 arguments:
 *		1. ArrayList<HashMap<String, String>> testList - a list of all the instances to be used for testing
 *		2. String pathStr - path to the Structure arff file to be used for testing
 *		3. FilteredClassifier fc - the FilteredClassifier generated after training 
 *		4. HashMap<String, Boolean> attrib - HashMap of attributes to be included during testing.
 **/
package classification;