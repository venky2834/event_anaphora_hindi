/**
 * Project : Event Anaphora Resolution
 * 
 * Project Mentor : Praveen Dhakwale
 * 
 * Author : Venkatesh Shukla, Anjali Jain
 * 
 * Language Technology and Research Center , Internatinal Institute of Information Technology
 * 
 * This Package contains the classes used in the Rule based Resolution of Event Anaphora.
 * 1. AntecedentStats.java -  this class prints the sentence distance and anaphora and antecedent sentence_id and chunk_name
 * 2. EventFiles.java - this class prints out the number of anaphora and their sentence id
 * 3. LastVerb.java - this class calculates the number of instances when the last verb is the referent excluding the cases of yeh - ki
 *
 **/

package ruleresolution;