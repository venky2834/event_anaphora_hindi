package tools;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PrintAllNodes 
{
	public static void Print(HashMap<String, Node> nMap) 
	{
		Set<String> keys = nMap.keySet();
		Iterator<String> iter = keys.iterator();
		System.out.print("Name");
		System.out.print("\t");
		System.out.print("Drel");
		System.out.print("\t");
		System.out.print("Parent");
		System.out.print("\t");
		System.out.print("POS");
		System.out.print("\t");
		System.out.print("Semprop");
		System.out.print("\t");
		System.out.print("Stype");
		System.out.print("\t");
		System.out.print("Vtype");
		System.out.print("\t");
		System.out.print("Child");
		System.out.print("\t");
		System.out.print("NumEle");
		System.out.print("\t");
		System.out.print("Wordlist");
		System.out.print("\t");
		System.out.print("WordFeats");
		System.out.print("\t");
		System.out.println();
		while(iter.hasNext())
		{
			//System.out.println(nMap.get(iter.next()));
			
			Node n = nMap.get(iter.next());
			System.out.print(n.getName());
			System.out.print("\t");
			System.out.print(n.getDrel());
			System.out.print("\t");
			System.out.print(n.getParent());
			System.out.print("\t");
			System.out.print(n.getPOS());
			System.out.print("\t");
			System.out.print(n.getSemprop());
			System.out.print("\t");
			System.out.print(n.getStype());
			System.out.print("\t");
			System.out.print(n.getVoicetype());
			System.out.print("\t");
			System.out.print(n.getChildren());
			System.out.print("\t");
			System.out.print(n.getNumEle());
			System.out.print("\t");
			System.out.print(n.getWordlist());
			System.out.print("\t");
			System.out.print(n.getWordFeatures());
			System.out.print("\t");
			System.out.println();
			
		}
	}
}
