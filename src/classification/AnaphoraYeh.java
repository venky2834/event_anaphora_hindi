package classification;

import java.util.ArrayList;
import java.util.HashMap;

import tools.ClassAttributeList;
import weka.classifiers.meta.FilteredClassifier;

public class AnaphoraYeh 
{
	// this files both test and trains on three different sets to get results of classification of anaphora with root yeh
	public static void main(String[] args) throws Exception
	{
		GenerateInstanceList gil = new GenerateInstanceList("annotated");
		ArrayList<HashMap<String,String>> allInstances = gil.instanceList;
		
		HashMap<String, Boolean> attrib = ClassAttributeList.get();
		String pathToArff = "arff/classify_train.arff";
		String pathStr = "arff/classify_structure.arff";
		
		ArrayList<HashMap<String,String>> setA = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> setB = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,String>> setC = new ArrayList<HashMap<String,String>>();
		
		int size = allInstances.size();
		for(int i = 0; i < size; i++)
		{
			if(i <= size/3) setA.add(allInstances.get(i));
			else if(i > size/3 && i <= 2*size/3) setB.add(allInstances.get(i));
			else if(i > 2*size/3) setC.add(allInstances.get(i));
			
		}
		ArrayList<HashMap<String,String>> trainSet;
		ArrayList<HashMap<String,String>> testSet;
		Train train;
		FilteredClassifier fc;
		Test test;
		
		float precision = 0, recall = 0, accuracy = 0, fscore = 0;
		
		/**Training and Testing 1st Set**/
		trainSet = new ArrayList<HashMap<String,String>>();
		testSet = new ArrayList<HashMap<String,String>>();
		
		trainSet.addAll(setA);
		trainSet.addAll(setB);
		testSet.addAll(setC);
		
		train = new Train(trainSet,pathToArff, pathStr, attrib);
		fc = train.fc;
		test = new Test(testSet, pathStr, fc, attrib);
		
		precision += test.precision;
		recall += test.recall;
		accuracy += test.accuracy;
		fscore += test.fscore;
		
		/**Training and Testing 2nd Set**/
		trainSet = new ArrayList<HashMap<String,String>>();
		testSet = new ArrayList<HashMap<String,String>>();
		
		trainSet.addAll(setB);
		trainSet.addAll(setC);
		testSet.addAll(setA);
		
		train = new Train(trainSet,pathToArff, pathStr, attrib);
		fc = train.fc;
		test = new Test(testSet, pathStr, fc, attrib);
		
		precision += test.precision;
		recall += test.recall;
		accuracy += test.accuracy;
		fscore += test.fscore;
		
		/**Training and Testing 3rd Set**/
		trainSet = new ArrayList<HashMap<String,String>>();
		testSet = new ArrayList<HashMap<String,String>>();
		
		trainSet.addAll(setC);
		trainSet.addAll(setA);
		testSet.addAll(setB);
		
		train = new Train(trainSet,pathToArff, pathStr, attrib);
		fc = train.fc;
		test = new Test(testSet, pathStr, fc, attrib);
		
		precision += test.precision;
		recall += test.recall;
		accuracy += test.accuracy;
		fscore += test.fscore;
		
		// Get Average values and print the result
		precision /= 3;
		recall /= 3;
		accuracy /= 3;
		fscore /= 3;
		
		System.out.println("\nAverage Values :");
		System.out.println("Precision :\t" + precision);
        System.out.println("Recall :\t" + recall);
        System.out.println("Accuracy :\t" + accuracy);
        System.out.println("F-Score :\t" + fscore);
	}
}
