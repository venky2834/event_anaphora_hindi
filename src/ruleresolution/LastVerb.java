package ruleresolution;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import tools.Node;
import tools.ProcessFile;
import tools.Tree;

public class LastVerb 
{
	// this class calculates the number of instances when the last verb is the referent excluding the cases of yeh - ki
	public static void main(String[] args) 
	{
		// All the files are present in this directory
		String path = "annotated";
		File f1 = new File(path);
		
		String[] fileList = f1.list();
		
		int right = 0;
		int wrong = 0;
		int cataphora = 0;
		
		for(int i = 0; i < fileList.length; i++)
		{
			// For a single file
			String filename = fileList[i];
			//System.out.println(filename);
			File f2 = new File(f1.getAbsoluteFile() + "/" + filename);
			ProcessFile pf = new ProcessFile(f2);
			
			ArrayList<Integer> keys = pf.Keys;
			HashMap<Integer, Tree> oneFile = pf.ProcessedFile;
			
			for(int j = 0; j < keys.size(); j++)
			{
				// for a single sentence
				int sent_id = keys.get(j);
				//System.out.println("Sentence :" + sent_id);
				Tree stree = oneFile.get(sent_id);
				// Analyse the tree to get all the features required.
				
				ArrayList<Node> nodeList = stree.findAll("reftype", "E");
				
				for(int k = 0; k < nodeList.size(); k++) 
				{
					Node ana_node = nodeList.get(k);
					HashMap<String, String> word = ana_node.getWordOfType("PRP");
					//System.out.println(word.get("ref"));
					int ref_sent_id = 0;
					String ref_chunk_name = null;
					
					int ref_sent_id_2 = 0;
					String ref_chunk_name_2 = null;
					
					String ante = word.get("ref");
					if(ante.contains(".."))
					{
						Scanner scan = new Scanner(ante);
						scan.useDelimiter("%|/|,");
						ArrayList<String> al = new ArrayList<String>();
						while(scan.hasNext()) al.add(scan.next());
						scan.close();
						ref_sent_id = Integer.valueOf(al.get(1));
						ref_chunk_name = al.get(2);
						if(ante.contains("/"))
						{
							ref_sent_id_2 = Integer.valueOf(al.get(4));
							ref_chunk_name_2 = al.get(5);
						}
					}
					else
					{
						ref_sent_id = sent_id;
						if(ante.contains("/"))
						{	
							Scanner sc = new Scanner(ante);
							sc.useDelimiter("/");
							ArrayList<String> al = new ArrayList<String>();
							while(sc.hasNext()) al.add(sc.next());
							sc.close();
							ref_sent_id_2 = sent_id;
							ref_chunk_name = al.get(0);
							ref_chunk_name_2 = al.get(1);
						}
						else
						{
							ref_chunk_name = ante;
						}
					}
					
					int sent_dist = sent_id - ref_sent_id;
					
					int b = 0;
					if((sent_dist) == 0)
					{
						b = 1;
						ArrayList<String> children = ana_node.getChildren();
						for(int l = 0; l < children.size(); l++)
						{
							Node child = stree.getNode(children.get(l));
							if(child.getPOS().equals("CCP") && child.getDrel().equals("rs"))
								{
									b = 2;
									break;
								}
						}
					}
					else b = 3;
					
					Tree prevTree = oneFile.get(sent_id - 1);
					// Takes care of missing sentence id
					if(prevTree == null) prevTree = oneFile.get(sent_id - 2);
					if(prevTree == null) prevTree = oneFile.get(sent_id - 3);
					if(prevTree == null) prevTree = oneFile.get(sent_id - 4);
					
					if(b == 1 || b == 3)
					{
						int ana_posn = ana_node.getPosn();
						String[] result = findNearestVerb(ana_posn, stree, prevTree);
						int near_sent_id = Integer.valueOf(result[0]);
						String near_chunk_name = result[1];
						
						System.out.printf("%d\t%d\t%d\t%s\t\t%d\t%s\t", sent_id, sent_dist, ref_sent_id, ref_chunk_name, near_sent_id, near_chunk_name);
						
						if(ref_sent_id == near_sent_id)
						{
							if(ref_chunk_name.equals(near_chunk_name))
							{
								right++;
								System.out.println("right");
							}
							else 
								{
									wrong++;
									System.out.println("wrong - same");
								}
						}
						else 
							{
								wrong++;
								System.out.println("wrong - prev");
							}
					}
				}
			}
		}
		System.out.printf("%s\t%d\n", "right", right);
		System.out.printf("%s\t%d\n", "wrong", wrong);
		
	}

	private static String[] findNearestVerb(int ana_posn, Tree stree, Tree prevTree) 
	{
		HashMap<Integer, Node> posnMap = stree.getPosnMap();
		
		String nearest_sent_id  = null;
		String nearest_chunk_name = null;
		
		Node nearest_node = new Node();
		int x = 0;
		
		for(int i = ana_posn; i > 0; i--)
		{
			Node n = posnMap.get(i);
			if(n.getPOS().contains("VG"))
			{
				nearest_node = n;
				x = 1;
				break;
			}
		}
		
		if(x == 0)
		{
			posnMap = prevTree.getPosnMap();
			
			int size = posnMap.size();
			
			for(int i = (size-1); i > 0; i++)
			{
				Node n = posnMap.get(i);
				//PrintNode.print(n);
				if(n.getPOS().contains("VG"))
				{
					nearest_node = n;
					x = 2;
					break;
				}
			}
		}
		

		nearest_chunk_name = nearest_node.getName();
		
		if(x == 1)	nearest_sent_id = String.valueOf(stree.getSentenceID());
		
		else if(x == 2)	nearest_sent_id = String.valueOf(prevTree.getSentenceID());
		
		String[] result = new String[2];
		
		result[0] = nearest_sent_id;
		result[1] = nearest_chunk_name;
		
		return result;
	}
}