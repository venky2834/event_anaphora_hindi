/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package event;
import java.io.*;
import java.util.*;
/**
 *
 * @author dell
 */
public class Distance1 {
    public static void main(String args[]) throws FileNotFoundException, IOException
    {
        String newline=null,lastvb=null,tag=null,lineoflastvb=null,sentenceid=null,idofreferent=null,sidlastvb=null,currentid=null,name=null;
        int size1=0,size2=0,size3=0,last=0,distance=0,currentidint=0,tot=0,post=0,z=0;
        ArrayList<String> refs=new ArrayList<String>();
        HashMap<Integer,Integer> dist=new HashMap<Integer,Integer>();//stores distance as the key and their corresponding counts as the object
        
        int actualevents=0,reflastvb=0,refasccp=0,lastandccp=0,j=0;
        boolean modified=false;
        String dirname="annotated";
        File directory=new File(dirname);
        File allfiles[]=directory.listFiles();
         for(int i=0;i<allfiles.length;i++)
         {
            FileReader fr=new FileReader(allfiles[i]+"");
            BufferedReader br = new BufferedReader(fr);
            newline=br.readLine();
            TreeMap<Integer, ArrayList<String>> fileverbs=new TreeMap<Integer, ArrayList<String>>();
            while(newline!=null)
            {
                if(newline.contains("<Sentence id="))
                {
                     ArrayList<String> sentverbs = new ArrayList<String>();
                     int ind=newline.indexOf("<Sentence id=")+14;
                     currentid=newline.substring(ind,newline.indexOf('\"', ind));
                     currentidint=Integer.parseInt(currentid);
                     //System.out.println(allfiles[i]+"");
                     //System.out.println("current id:"+currentid);
                     newline=br.readLine();
                     boolean vbpresent=false;
                     while(!newline.contains("</Sentence>"))
                     {
                         if(newline.contains("(("))
                        {

                            Getvalues gv=new Getvalues();
                            tag=gv.getTag(newline);
                            if((tag.contains("VGF") || tag.contains("VGNF")|tag.contains("VGNN"))&& !tag.contains("NULL"))//if the chunk is a verb chunk
                            {
                                name=gv.getName(newline);
                                sentverbs.add(name);//add the name of verb chunk to sentverbs
                                vbpresent=true;
                            }

                        }
                         else if(newline.contains("reftype='E'"))
                         {
                            distance=0;
                            modified=false;
                            Getvalues gv=new Getvalues();
                            refs=gv.getReferent(newline);//retrieves all the referents of the anaphore into an arraylist
                            int x=refs.size();
                            if(refs.size()>1)
                              z++;
                            String referent=refs.get(x-1);//get the last referent(closest)
                            //System.out.println(allfiles[i]+"");
                            //System.out.println(referent);
                            if(referent.contains("%"))//if closest referent lies in other sentence
                            {

                                int i1=referent.indexOf('%')+1;
                                int i2=referent.indexOf('%',i1);
                                String sid=referent.substring(i1,i2);//get the sentence id of the referent
                                int sidint=Integer.parseInt(sid);//convert sentence id to integer
                                currentidint=Integer.parseInt(currentid);
                                //System.out.println(sid);
                                String refverb=referent.substring(i2+1);//get the name of the referent
                                if(fileverbs.containsKey(sidint))
                                {
                                    ArrayList<String> refsent = new ArrayList<String>();
                                    refsent=fileverbs.get(sidint);
                                    int size=refsent.size();
                                    int index=refsent.indexOf(refverb);
                                    //System.out.println(sidint+" "+refsent+" "+refverb+" "+currentidint);
                                    //System.out.println("size:"+size+" index:"+index);
                                    //System.out.println(sidint+""+refsent+"\t"+refverb);
                                   /* if(index==-1)
                                    {
                                        System.out.println(allfiles[i]+"");
                                    System.out.println(refsent+" "+refverb);
                                    System.out.println("index="+index);

                                    }*/
                                    if(index!=-1)
                                    {
                                    distance=distance+size-index-1;
                                            if(sidint!=fileverbs.lastKey())
                                            {
                                    int nextkey=fileverbs.higherKey(sidint);





                                    while(nextkey!=fileverbs.lastKey())
                                    {

                                        distance=distance+fileverbs.get(nextkey).size();

                                        nextkey=fileverbs.higherKey(nextkey);
                                        //System.out.println("nextkey:"+nextkey);
                                    }
                                    if(nextkey==fileverbs.lastKey())
                                    {
                                        distance=distance+fileverbs.get(fileverbs.lastKey()).size();
                                    }
                                    }
                                    distance=distance+sentverbs.size();
                                    if(!dist.containsKey(distance))
                                    {
                                        dist.put(distance, 1);
                                        
                                    }
                                    else
                                    {
                                        int c=dist.get(distance);
                                        dist.put(distance, ++c);
                                    }


                                    modified=true;
                                    }
                                }
                                if(!fileverbs.containsKey(sidint))
                                {
                                   // System.out.println(allfiles[i]+"");
                                   // System.out.println(++j+" "+"sid ref:"+sidint+" current id:"+currentidint+" "+fileverbs.entrySet());
                                }


                            }
                            else
                            {

                                String refverb=referent;
                                //System.out.println("*"+sentverbs+" "+refverb);
                                int v=sentverbs.indexOf(refverb);
                                //System.out.println(v);
                                if(v!=-1)
                                {
                                distance=distance+sentverbs.size()-v-1;
                                if(!dist.containsKey(distance))
                                    {
                                        dist.put(distance, 1);

                                    }
                                    else
                                    {
                                        int c=dist.get(distance);
                                        dist.put(distance, ++c);
                                    }

                                modified=true;
                                }
                                /*if(v==-1)
                                {
                                    System.out.println(allfiles[i]+"");
                                    System.out.println(++j+")"+sentverbs+" "+refverb+" "+currentidint+" distance:"+distance);
                                    post++;
                                }*/
                                //System.out.println(refverb);
                            }

                           // System.out.println(++j+"::"+"Distance="+distance);
                            //System.out.println(allfiles[i]+"");
                         }
                          newline=br.readLine();
                     }
                     if(newline.contains("</Sentence>"))//if end of sentence observed
                    {
                        if(!vbpresent)
                    {

                        sentverbs.add(null);//if no verb chunks present in the sentence

                    }

                        fileverbs.put(currentidint, sentverbs);//put all the verb chunks stored in an array to the treemap with sentence id as the key



                    }
                }
                newline=br.readLine();
            }

         }
        //System.out.println(post);

       System.out.println(dist);
    }

}
