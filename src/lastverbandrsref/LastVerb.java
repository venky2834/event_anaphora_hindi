/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package event;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author dell
 */
public class LastVerb {
    public static void main(String args[]) throws FileNotFoundException, IOException
    {
        String newline=null,lastvb=null,tag=null,lineoflastvb=null,sentenceid=null,idofreferent=null,sidlastvb=null;
        int size1=0,size2=0,size3=0,yahe=0,yahdem=0,yahunk=0;
        ArrayList<String> refs=new ArrayList<String>();
        int actualevents=0,reflastvb=0,refasccp=0,lastandccp=0,j=0;
        String dirname="annotated";
        File directory=new File(dirname);
        File allfiles[]=directory.listFiles();
        for(int i=0;i<allfiles.length;i++)
        {
            FileReader fr=new FileReader(allfiles[i]+"");
            
            BufferedReader br = new BufferedReader(fr);
            newline=br.readLine();
            while(newline!=null)
            {
                
                if(newline.contains("<Sentence id="))
                {
                    int ind=newline.indexOf("<Sentence id=")+14;
                    sentenceid=newline.substring(ind,newline.indexOf('\"', ind));//retrieve sentence id of the sentence
                    //System.out.println(sentenceid);
                }
                if(newline.contains("(("))
               {
                    Getvalues gv=new Getvalues();
                 
                 tag=gv.getTag(newline);//get pos tag of chunk
                 
                 if(tag.contains("VG") && !tag.contains("NULL"))//check if chunk is a verb chunk
                 {
                     
                     gv=new Getvalues();
                     lastvb=gv.getName(newline);//store name of the verb chunk
                     sidlastvb=sentenceid;//store sentence id of the verb chunk
                     lineoflastvb=newline;//store the chunkline
                     //System.out.println(lastvb);
                 }
                
               }
                else if(newline.contains("reftype='E'"))
                {
                    actualevents++;
                    Getvalues gv=new Getvalues();
                    //System.out.println(allfiles[i]+"");
                    refs=gv.getReferent(newline);//retrive referents of the word marked reftype='E' in array refs
                    
                   
                    for(int k=0;k<refs.size();k++)
                    {
                        String ref=refs.get(k);
                        if(ref.contains("%"))//if referent lies in other sentence(previous)
                        {
                            int p=ref.indexOf('%');
                            int ind=ref.lastIndexOf('%');
                            idofreferent=ref.substring(p+1,ind);//store the sentence id of referent
                            
                            String verb=ref.substring(ind+1);//store the name of referent
                            
                            if(verb.equals(lastvb) && sidlastvb.equals(idofreferent))//if the name and sentence of the referent verb is same as that of the one observed last
                            {
                               // System.out.println(allfiles[i]+"");

                                //System.out.println(++j+"Sentence id:"+sentenceid+" id of referent:"+idofreferent);
                           //
                                //System.out.println(++j+"sentence id of last verb:"+sidlastvb+" last verb:"+lastvb+" "+sentenceid+" "+ verb);
                            //System.out.println(ref);
                                //System.out.println(++j+":"+verb+"\t"+lastvb);
                                reflastvb++;
                                
                            }
                            String fname=allfiles[i]+"";
                        
                        }
                        else if(ref.equals(lastvb) && sentenceid.equals(sidlastvb))//if referent name is same as that of the name of the last verb chunk and the referent lies in the current sentence
                        {
                            //System.out.println("\n"+ref);
                            //System.out.println(allfiles[i]+"");
                            //System.out.println(++j+" id of referent:"+idofreferent+" sentence id of last verb:"+sidlastvb+" last verb:"+lastvb);
                            //System.out.println(++j+"Sentence id:"+sentenceid+" sentence id of last verb:"+sidlastvb+" last verb:"+lastvb);
                            //System.out.println(++j+":"+ref+"\t"+lastvb);
                            /*System.out.println(allfiles[i]+"");
                            System.out.println(++j+"sentence id of last verb:"+sidlastvb+" last verb:"+lastvb+" "+sentenceid+" "+ ref);*/
                            reflastvb++;
                            
                        }
                        
                    }
                    
               
                }
               newline=br.readLine();
            }

        }
        
        int rsresolved=FindRs.getRS(dirname);//find out events having rs relation(yah & ki) and those rs relation having direct descendent of "ki" as referent
        System.out.println("Total events(reftype='E'):"+actualevents);
        System.out.println("Total events excluding rs:"+(actualevents-85));
        System.out.println("referent as last verb:"+reflastvb);
        System.out.println("Percentage(excluding rs):"+(float)reflastvb*100/(actualevents-85));
        System.out.println("RS resolved correctly:"+rsresolved);
        System.out.println("Events resolved correctly:"+(float)(reflastvb+rsresolved)*100/actualevents);
        

    }
   }
