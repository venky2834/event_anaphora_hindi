/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package event;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author dell
 */
public class FindRs {
    public static int getRS(String path) throws FileNotFoundException, IOException
    {
        String newline=null,sentenceid=null,nameofchunk=null,sidofyah=null,start=null,name=null,yahline=null,yahstart=null,verbchunkline=null,kiname=null,yl=null,mainverbchunkline=null;
        boolean yah=false,yahandki=false;
        int rsevents=0,j=0,childofki=0,totalevents=0;
        File directory=new File(path);
        File allfiles[] = directory.listFiles();
        for(int i=0;i<allfiles.length;i++)
        {
            FileReader fr=new FileReader(allfiles[i]+"");

            BufferedReader br = new BufferedReader(fr);
            newline=br.readLine();
            while(newline!=null)
            {
               if(newline.contains("<Sentence id="))
                {
                   yah=false;
                   yahandki=false;//make existance of "yah" and "ki" rs relation as false
                    int ind=newline.indexOf("<Sentence id=")+14;
                    int x=newline.indexOf('\"', ind);
                    sentenceid=newline.substring(ind, x);//store sentence id
                    
                    //System.out.println(sentenceid);
                }
               if(newline.contains("(("))
               {
                    Getvalues gv=new Getvalues();
                    start=newline;
                    name=gv.getName(newline);//retrieve name of the chunk
                    if(name.contains("VG") && !name.contains("NULL"))//if chunk is a verb chunk
                    {
                        verbchunkline=newline;//store the chunkline of verbchunk
                        
               if(yahandki)//if "yah" and "ki" exist having an rs relation
               {
                   gv=new Getvalues();
                   ArrayList<String> referents=gv.getReferent(yl);//get referents of the anaphor(line stored in y1)
                   String nameofverb=gv.getName(verbchunkline);//get name of the current verbchunk

                   
                   if(verbchunkline.contains("drel='ccof:"+kiname+"'") && nameofverb.equals(referents.get(referents.size()-1)))//select the last referent if more than 1 referents and check if it has ccof direct relation with "ki" chunk stored earlier
                   {
                       childofki++;
                    //System.out.println(allfiles[i]+"");
                   //System.out.println(j+++":"+verbchunkline+"  "+kiname);
                       
                   }
                     else if(nameofverb.equals(referents.get(referents.size()-1)))//if the verb chunk is the referent but it does not have ccof relation with "ki"
                   {
                  System.out.println(allfiles[i]+"");
                    System.out.println("name of verb:"+nameofverb+" referent:"+referents.get(referents.size()-1));
                   System.out.println(j+++":"+yl+" "+verbchunkline+"  "+kiname);


                    }
                    else if(verbchunkline.contains("drel='ccof:"+kiname+"'") && !nameofverb.equals(referents.get(referents.size()-1)))//if the verb chunk has ccof relation with "ki" but is not the referent of anaphor(considering last referent)
                   {
                     // System.out.println(allfiles[i]+"");
                       //System.out.println("name of verb:"+nameofverb+" referent:"+referents.get(referents.size()-1));
                         //System.out.println(j+++":"+yl+" "+verbchunkline+"  "+kiname);

                    }
                   
                  
               }

                    }
               }
               if(newline.contains("reftype='E'"))
               {
                    totalevents++;
               }
               if(newline.contains("	यह	") && newline.contains("reftype='E'"))
               {
                   
                   yahstart=start;//store the chunk line of "yah"
                   nameofchunk=name;//store name of the chunk
                   sidofyah=sentenceid;//store sentence id of "yah"
                   yahline=newline;//store the line which contains anaphor yah
                   yah=true;
               }
                   
               
               if(newline.contains("	कि	") && yah)
               {
                 if(start.contains("drel='rs:"))
                 {
                     
                     int ind=start.indexOf("drel='rs:")+9;

                     String reltn=start.substring(ind,start.indexOf('\'', ind));
                     //System.out.println(sentenceid+"\t"+sidofyah);
                     if(sentenceid.equals(sidofyah) && reltn.equals(nameofchunk))//check for drel=rs with the yah chunk
                     {
                         Getvalues gv=new Getvalues();
                         kiname=gv.getName(start);
                         yl=yahline;//if it has rs relation with key store the yahline in y1
                         
                         
                         //System.out.println(sentenceid+" "+sidofyah);
                         //System.out.println(allfiles[i]+"");
                         //System.out.println(j+++yahstart+"\n"+yahline+"\n"+start+"\n"+newline);
                         rsevents++;

                         yahandki=true;//make yahandki true to denote that rs relation exists in the current sentence
                     }
                 }
               }
               
               newline=br.readLine();
            }

        }
        /*System.out.println("Total events:"+totalevents);*/
        System.out.println("rs events:"+rsevents);
        System.out.println("no of rs yah having direct descendent of ki as referent:"+childofki);
        System.out.println("% of rs events resolved correctly:"+((float)childofki*100/rsevents)+"%");
        return childofki;
    }

}
