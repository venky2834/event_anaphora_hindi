package classification;

import java.util.ArrayList;
import java.util.HashMap;

import tools.AllowedDrel;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Test 
{
	public float precision;
	public float recall;
	public float accuracy;
	public float fscore;
	
	public Test(ArrayList<HashMap<String, String>> testList, String pathStr, FilteredClassifier fc, HashMap<String, Boolean> attrib) throws Exception
	{

		DataSource test_source = new DataSource(pathStr);
		
		//Instances test = test_source.getDataSet();
        Instances test = test_source.getStructure();
        
		int cIdx_train = test.numAttributes() - 1;
		test.setClassIndex(cIdx_train);

		int evPredAsEv = 0;
		int enPredAsEv = 0;
		int evPredAsEn = 0;
		int enPredAsEn = 0;
		
		ArrayList<String> allowedDrel = AllowedDrel.get();
		
		for(int i = 0; i < testList.size(); i++) 
        {
        	Instance instance = new Instance(test.numAttributes());
        	instance.setDataset(test);
        	
        	HashMap<String, String> hm = testList.get(i);
        	
        	int k = 1;
        	
        	
        	//System.out.printf("%s,", hm.get("anaphora"));
        	instance.setValue(test.attribute(0), hm.get("anaphora"));
        	
        	if(attrib.get("isAlone"))
        	{
        		//System.out.printf("%s,", hm.get("isAlone"));
        		instance.setValue(test.attribute(k), hm.get("isAlone"));
        		k++;
        	}
        	
        	if(attrib.get("noChild"))
        	{
        		//System.out.printf("%s,", hm.get("noChild"));
        		instance.setValue(test.attribute(k), hm.get("noChild"));
        		k++;
        	}
        	
        	if(attrib.get("drel"))
        	{
        		if(allowedDrel.contains(hm.get("drel")))
        		{	
        			//System.out.printf("%s,", hm.get("drel"));
        			instance.setValue(test.attribute(k), hm.get("drel"));
        		}
        		else instance.setValue(test.attribute(k),"others");
        		k++;
        	}
        	
        	if(attrib.get("next_word"))
        	{
        		//System.out.printf("%s,", hm.get("next_word"));
        		if(hm.get("next_word") == null) instance.setValue(test.attribute(k),"null");
        		else instance.setValue(test.attribute(k), hm.get("next_word"));
        		k++;
        	}
        	
        	if(attrib.get("semprop"))
        	{
        		//System.out.printf("%s,", hm.get("semprop"));
        		if(hm.get("semprop") == null) instance.setValue(test.attribute(k),"null");
        		else instance.setValue(test.attribute(k), hm.get("semprop"));
        		k++;
        	}
        	
        	if(attrib.get("case"))
        	{
        		//System.out.printf("%s,", hm.get("case"));
        		if(hm.get("case") == null) instance.setValue(test.attribute(k),"null");
        		else instance.setValue(test.attribute(k), hm.get("case"));
        		k++;
        	}
        	
        	if(attrib.get("num"))
        	{
        		//System.out.printf("%s,", hm.get("num"));
        		if(hm.get("num") == null) instance.setValue(test.attribute(k), "null");
        		else instance.setValue(test.attribute(k), hm.get("num"));
        		k++;
        	}
        	
        	if(attrib.get("gender"))
        	{
        		//System.out.printf("%s,", hm.get("gender"));
        		if(hm.get("gender") == null) instance.setValue(test.attribute(k), "null");
        		else instance.setValue(test.attribute(k), hm.get("gender"));
        		k++;
        	}
        	
        	if(attrib.get("TAM"))
        	{
        		//System.out.printf("%s,", hm.get("TAM"));
        		if(hm.get("TAM") == null) instance.setValue(test.attribute(k), "null");
        		else instance.setValue(test.attribute(k), hm.get("TAM"));
        		k++;
        	}
        	
        	
        	
        	double pred = fc.classifyInstance(instance);
        	double conf[] =  fc.distributionForInstance(instance);
        	
        	String actual = hm.get("class");
        	String predicted = test.classAttribute().value((int) pred);
        	
        	//System.out.printf("%s\t", actual);
        	//System.out.printf("%s\t", predicted);
        	//System.out.printf("%f\t", conf[0]);
        	
        	//if(predicted.equals(actual)) System.out.printf("%s\n","Correct");
        	//else System.out.printf("%s\n","Wrong");
        	
        	if(predicted.equals("event"))
        	{
        		if(actual.equals("event"))
        			evPredAsEv++;
        		else
        			enPredAsEv++;
          	}
        	else
        	{
        		
				if(actual.equals("event"))
        			evPredAsEn++;
        		else
        			enPredAsEn++;
        	}
        }
        precision = 100*((float)evPredAsEv/(evPredAsEv + enPredAsEv));
        recall = 100*((float)evPredAsEv/(evPredAsEv + evPredAsEn));
        accuracy = 100*((float)(evPredAsEv + enPredAsEn)/(evPredAsEv + enPredAsEn + enPredAsEv + evPredAsEn));
        fscore = ((float) (2*precision*recall)/(precision + recall));
        
        int totalInstances = evPredAsEv + enPredAsEv + evPredAsEn + enPredAsEn;
        
        System.out.println("\nResult of Testing :\n");
        System.out.println("Number of Events predicted as Events :\t\t" + String.valueOf(evPredAsEv));
        System.out.println("Number of Events predicted as Entities :\t" + String.valueOf(evPredAsEn));
        System.out.println("Number of Entities predicted as Events :\t" + String.valueOf(enPredAsEv));
        System.out.println("Number of Entities predicted as Entities :\t" + String.valueOf(enPredAsEn));
        System.out.println("Number of Instances :\t\t\t\t" + String.valueOf(totalInstances));
        System.out.println();
        
        System.out.println("Precision :\t" + precision);
        System.out.println("Recall :\t" + recall);
        System.out.println("Accuracy :\t" + accuracy);
        System.out.println("F-Score :\t" + fscore);
        
	}
}
