package classification;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import tools.AllowedDrel;
import weka.classifiers.meta.FilteredClassifier;

public class Train 
{
	public FilteredClassifier fc = new FilteredClassifier();
	
	public Train(ArrayList<HashMap<String, String>> trainList, String pathToArff, String pathStr, HashMap<String, Boolean> attribs) throws Exception
	{
		FileOutputStream fos1 = new FileOutputStream(pathToArff);
		FileOutputStream fos2 = new FileOutputStream(pathStr);
		
		OutputStreamWriter osw1 = new OutputStreamWriter(fos1);
		OutputStreamWriter osw2 = new OutputStreamWriter(fos2);
		
		osw1.write("@relation anaphora-classification\n\n");
		osw2.write("@relation anaphora-classification\n\n");
		
		osw1.write("@attribute anaphora string\n");
		osw2.write("@attribute anaphora string\n");
		
		if(attribs.get("isAlone")) 
			{
				osw1.write("@attribute isalone {true,false}\n");
				osw2.write("@attribute isalone {true,false}\n");
			}
		
		if(attribs.get("noChild")) 
			{
				osw1.write("@attribute noChild {true,false}\n");
				osw2.write("@attribute noChild {true,false}\n");
			}
		
		if(attribs.get("drel")) 
			{
				osw1.write("@attribute drel {");
				osw2.write("@attribute drel {");
				
				ArrayList<String> al = AllowedDrel.get();
				for(int j = 0; j < al.size(); j++)
				{
					osw1.write(al.get(j) + ",");
					osw2.write(al.get(j) + ",");
				}
				osw1.write("others}\n");
				osw2.write("others}\n");
			}
		
		if(attribs.get("next_word")) 
			{
				osw1.write("@attribute next_word string\n");
				osw2.write("@attribute next_word string\n");
			}
		
		if(attribs.get("semprop")) 
			{
				osw1.write("@attribute semprop {h,in,null,rest}\n");
				osw2.write("@attribute semprop {h,in,null,rest}\n");
			}
		
		if(attribs.get("case")) 
			{
				osw1.write("@attribute case {any,d,null,o}\n");
				osw2.write("@attribute case {any,d,null,o}\n");
			}
		
		if(attribs.get("num")) 
			{
				osw1.write("@attribute num {any,null,pl,sg}\n");
				osw2.write("@attribute num {any,null,pl,sg}\n");
			}
		
		if(attribs.get("gender")) 
			{
				osw1.write("@attribute gender {any,f,m,null}\n");
				osw2.write("@attribute gender {any,f,m,null}\n");
			}
		
		if(attribs.get("TAM")) 
			{
				osw1.write("@attribute TAM {0,null,का,के,को,ने,में,से}\n");
				osw2.write("@attribute TAM {0,null,का,के,को,ने,में,से}\n");
			}
		
		osw1.write("@attribute class {event, entity}\n");
		osw2.write("@attribute class {event, entity}\n");
		
		osw1.write("\n@data\n");
		osw2.write("\n@data\n");
		
		osw2.flush();
		osw2.close();
		
		ArrayList<String> allowedDrel = AllowedDrel.get();
		
		for(int i = 0; i < trainList.size(); i++)
		{
			HashMap<String, String> instance = trainList.get(i);
			
			osw1.write(instance.get("anaphora") + ",");
			if(attribs.get("isAlone")) osw1.write(instance.get("isAlone") + ",");
			if(attribs.get("noChild")) osw1.write(instance.get("noChild") + ",");
			
			if(attribs.get("drel")) 
				{
				if(allowedDrel.contains(instance.get("drel"))) osw1.write(instance.get("drel") + ",");
				else	osw1.write("others" + ",");
				}
			
			if(attribs.get("next_word")) osw1.write(instance.get("next_word") + ",");
			if(attribs.get("semprop")) osw1.write(instance.get("semprop") + ",");
			if(attribs.get("case")) osw1.write(instance.get("case") + ",");
			if(attribs.get("num")) osw1.write(instance.get("num") + ",");
			if(attribs.get("gender")) osw1.write(instance.get("gender") + ",");
			if(attribs.get("TAM")) osw1.write(instance.get("TAM") + ",");
			osw1.write(instance.get("class") + "\n");
			
		}
		osw1.flush();
		osw1.close();
		
		MachineLearn ml = new MachineLearn(pathToArff);
		fc = ml.fc;
	}	
}