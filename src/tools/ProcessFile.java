package tools;


public class ProcessFile
{
	// Take a file as an arguement and gives back a hashmap with all the sentences present as Tree in it.
	// The respective sentence id are the keys
	public ArrayList<Integer> Keys = new ArrayList<Integer>();
	public HashMap<Integer,Tree> ProcessedFile = new HashMap<Integer,Tree>();
	
	public ProcessFile(File f)
	{
		ArrayList<String> oneFile = new ArrayList<String>();
		try 
		{
			FileInputStream fis = new FileInputStream(f);
			InputStreamReader isr = new InputStreamReader(fis,"UTF8");
			BufferedReader br = new BufferedReader(isr);
			
			String s;
			while((s = br.readLine()) != null)
			{
				if(s.equals("")) continue;
				oneFile.add(s);
			}
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		ArrayList<String> sentence = new ArrayList<String>();
		boolean collect = false;
		int sentence_id = 0;
		
		for(int i = 0; i < oneFile.size(); i++)
		{
			String s = oneFile.get(i);
			if(s.contains("\tSYM\t")) continue;
			if(s.contains("<Sentence id=")) 
			{
				collect = true;
				sentence = new ArrayList<String>();
				//System.out.println(s);
				Scanner sc = new Scanner(s);
				sc.useDelimiter("\">|=\"|='|'>|<| ");
				sc.next();sc.next();
				
				sentence_id = Integer.valueOf(sc.next());
				sc.close();
			}
			else if(s.contains("</Sentence>"))
			{
				//sentence.add(s);
				collect = false;
				//PrintAL(sentence);
				MakeTree mt = new MakeTree(sentence);
				Tree tree = mt.SentenceTree;
				tree.setSentenceID(sentence_id);
				ProcessedFile.put(sentence_id, tree);
			}
		
			if(collect) sentence.add(s);
		}
		
		
		Set<Integer> keyset = ProcessedFile.keySet();
		Keys = new ArrayList<Integer>(); 
		Iterator<Integer> it = keyset.iterator();
		while(it.hasNext()) Keys.add(it.next());
		
		// Now we have all the sentences as trees stored in a hashmap
		// Analyse these sentences to get required results
		// return processedFile;
	}

	private void PrintAL(ArrayList<String> sentence) 
	{
		for(int i = 0; i < sentence.size(); i++)
		System.out.println(sentence.get(i));
	}
}
