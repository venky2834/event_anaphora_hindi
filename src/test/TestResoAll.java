package test;

import java.util.ArrayList;
import java.util.HashMap;

import machineresolution.GenerateInstanceList;
import machineresolution.Test;
import machineresolution.Train;
import tools.ResolveAttributeList;
import weka.classifiers.meta.FilteredClassifier;

public class TestResoAll 
{
	public static void main(String [] args) throws Exception
	{
		String dir = "annotated";
		GenerateInstanceList gis = new GenerateInstanceList(dir);
		ArrayList<HashMap<String, String>> instanceList = gis.instanceList;
		
		String trainArff = "arff/resolve_train.arff";
		String structArff = "arff/resolve_structure.arff";
		
		HashMap<String, Boolean> attrib = ResolveAttributeList.get();
		
		Train train = new Train(instanceList, trainArff, structArff, attrib);
		
		FilteredClassifier fc = train.fc;
		
		Test test = new Test();
		test.main(args);
	}
}
