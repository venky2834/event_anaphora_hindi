package tools;

import java.util.HashMap;

/*
 * This class creates a boolean hashmap of attributes to be used during training and testing
 * The attributes to be used should be marked as true
 * and the rest as false
 * 
 */
public class ResolveAttributeList 
{
	public static HashMap<String, Boolean> get() 
	{
		HashMap<String, Boolean> attrib = new HashMap<String, Boolean>();

		attrib.put("ana_root", true);
		//attrib.put("ana_root", false);
		
		attrib.put("ana_drel", true);
		//attrib.put("ana_drel", false);
		
		//attrib.put("ana_semprop", true);
		attrib.put("ana_semprop", false);
		
		attrib.put("ana_next", true);
		//attrib.put("ana_next", false);
		
		attrib.put("ana_numEle", true);
		//attrib.put("ana_numEle", false);
		
		//attrib.put("ana_numChild", true);
		attrib.put("ana_numChild", false);
		
		attrib.put("ana_gender", true);
		//attrib.put("ana_gender", false);
		
		attrib.put("ana_num", true);
		//attrib.put("ana_num", false);
		
		attrib.put("ana_case", true);
		//attrib.put("ana_case", false);
		
		//attrib.put("ana_person", true);
		attrib.put("ana_person", false);
		
		//attrib.put("ana_TAM", true);
		attrib.put("ana_TAM", false);
		
		attrib.put("ref_word", true);
		//attrib.put("ref_word", false);
		
		//attrib.put("ref_root", true);
		attrib.put("ref_root", false);
		
		attrib.put("ref_drel", true);
		//attrib.put("ref_drel", false);
		
		//attrib.put("ref_semprop", true);
		attrib.put("ref_semprop", false);
		
		attrib.put("ref_numEle", true);
		//attrib.put("ref_numEle", false);
		
		//attrib.put("ref_numChild", true);
		attrib.put("ref_numChild", false);
		
		attrib.put("ref_gender", true);
		//attrib.put("ref_gender", false);
		
		attrib.put("ref_num", true);
		//attrib.put("ref_num", false);
		
		attrib.put("ref_case", true);
		//attrib.put("ref_case", false);
		
		//attrib.put("ref_person", true);
		attrib.put("ref_person", false);
		
		//attrib.put("ref_TAM", true);
		attrib.put("ref_TAM", false);
		
		attrib.put("sent_dis", true);
		//attrib.put("sent_dis", false);
		
		//attrib.put("verb_dis", true);
		attrib.put("verb_dis", false);
		
		
		return attrib;
	}
}
