package tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/* Given a sentence in form of ArrayList<String> this class returns a Tree containing the following information :
 * 
 * 1. Sentence ID - int
 * 2. Root Node of the tree - String
 * 3. HashMap of all Nodes of the tree with the chunk name as the keys - HashMap<String, Node>
 * 4. Keys - an ArrayList of all the chunk names.
 * 
 */

public class MakeTree 
{
	public Tree SentenceTree;
	public MakeTree(ArrayList<String> sentence)
	{
		HashMap<String, Node> nMap = new HashMap<String, Node>();
		ArrayList<String> al = new ArrayList<String>();
		ArrayList<String[]> chunklist = new ArrayList<String[]>();
		boolean collect = false;
		for(int i = 0; i < sentence.size(); i++)
		{
			String l = sentence.get(i);
			if(l.contains("((")) 
				{
					collect = true;
					al = new ArrayList<String>();
					
					Scanner scan = new Scanner(l);
					scan.useDelimiter("\t<|='|'>|' |:|\t");
					ArrayList<String> w = new ArrayList<String>();
					String [] ch_info = new String[2];
					while(scan.hasNext()) 
					{
						String str = scan.next();
						w.add(str);
						if(str.equals("drel")) 
						{
							scan.next();
							ch_info[1] = scan.next();
						}
					}
					ch_info[0] = w.get(4);
					scan.close();
					chunklist.add(ch_info);
				}
			else if(l.contains("))")) 
				{
					collect = false;
					//PrintAL(al);
					MakeNode mn = new MakeNode(al);
					Node n = mn.node;
					//PrintNode.print(n);
					nMap.put(n.getName(), n);
				}
			if(collect) al.add(l);
		}
		
		for(int i = 0; i < chunklist.size(); i++)
		{
			String name = chunklist.get(i)[0];
			//System.out.println(name);
			ArrayList<String> child_list = new ArrayList<String>();
			for(int j = 0; j < chunklist.size(); j++)
			{
				if(j == i) continue;
				String parent = chunklist.get(j)[1];
				//System.out.println("\t" + parent);
				if(name.equals(parent)) child_list.add(chunklist.get(j)[0]);
			}
			//System.out.println(child_list);
			nMap.get(name).setChildren(child_list);
		}
		SentenceTree = new Tree(nMap);
		//System.out.println(tree.getRoot());
		//PrintAllNodes.Print(nMap);
	}
	
}